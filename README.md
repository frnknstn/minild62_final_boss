## MiniLD 62 "Final Boss" - Inclination #

My intention is to make a Descent clone.

### Ideas and goals #

* Descent-like
* multiplayer (coop) base
* single level, one main (boss) enemy
* Aesthetic
    * Old-school textures
    * Reactive controls, lots of wobble
    * Bright
    * As flashy as my meagre skills allow
    * Game pretends to be loading from an old save file
* Required features
    * Lasers!
    * Missiles?
    * Boss monster
* Nice-to-have
    * Pickups
        * Weapon Upgrades
        * Ammo
    * Difficulty level
        * On easy, Boss dies regardless of health, when you expend your last ammo (fake epic :)
* Ideas
    * Weapon types
        * Laser cartridge
        * Homing missile
        * Dumb missile swarm
    * Boss design
        * ???

### Multiplayer base #

Part of my intention is to build upon a basic multiplayer engine in for practice. I found some YouTube preview tutorials for the new Unity 5 networking and implemented basic rigidbody state sending based on that.

For the actual project, let's see how long my dream of keeping the game network-transparent lasts.

### Abandoned ideas #

To start with, I breiefly followed the multiplayer Unity tutorial at:

http://www.paladinstudios.com/2013/07/10/how-to-create-an-online-multiplayer-game-with-unity/

However, that is technically obsolete in the latest Unity.