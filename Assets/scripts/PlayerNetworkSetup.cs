﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;


public class PlayerNetworkSetup : NetworkBehaviour {
    [SerializeField] Camera playerCamera;
    [SerializeField] AudioListener audioListener;
    [SerializeField] Camera headBob = null;

    private GameObject sceneCamera = null;

    private UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsController;
    private UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController rigidController;

    override public void OnStartLocalPlayer() {
        Debug.Log("Enabling local player");
        fpsController = GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
        rigidController = GetComponent<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController>();
        ConfigureAsLocalPlayer(true);
    }

    public void OnDestroy() {
        if (isLocalPlayer) {
            Debug.Log("Disabling local player");
            ConfigureAsLocalPlayer(false);
        }
    }

    void ConfigureAsLocalPlayer(bool active) {
        if (active) {
            sceneCamera = GameObject.Find("Scene Camera");
        }
        if (sceneCamera != null) {
            sceneCamera.SetActive(!active);
        }

        if (fpsController != null) {
            Debug.Log("fps");
            fpsController.enabled = active;
        }

        if (rigidController != null) {
            Debug.Log("rigid");
            rigidController.enabled = active;
        }

        playerCamera.enabled = active;
        audioListener.enabled = active;

        if (headBob != null) {
            headBob.enabled = active;
        }
    }
}
